import * as React from 'react';
import { Alert } from 'reactstrap';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import DeleteButton from '../containers/DeleteButton';
// import { Link } from 'react-router-dom';
import { ExpandedProduct, Promotion } from '../types/index';
import * as H from 'history';
import * as moment from 'moment';

export interface Props {
  location: H.Location;
}

export interface State {
  drink: ExpandedProduct;
  barName: string;
}

const weekDays = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

export default class ViewDeals extends React.Component<Props, State>{ // tslint:disable-line
  constructor(props: Props) {
    super(props);

  }

  componentWillMount() {
    this.setState({barName: this.props.location.state.barName, drink: this.props.location.state.drink});
  }
  deleteDeal(id: string) {
    alert(id);
  }
  renderDeals = () => {
    console.log(this.state.drink); //tslint:disable-line
    const deals = this.state.drink.promotions;

    return deals.map( (deal: Promotion) => {
      return (
         <div key={deal._id} >
           <Card>
             <CardContent>
               <Typography>
                 ${deal.price}
               </Typography>
               <Typography variant="headline">
                 <Typography>
                   {moment(deal.period.start).format('D.M.YYYY')}
                   -
                   {moment(deal.period.end).format('D.M.YYYY')}
                 </Typography>
                   {deal.days.map( (day: string) => {
                     return (
                       <Typography key={day}>
                         {weekDays[day]}
                         ,&nbsp;
                         {moment.utc().startOf('day').add(deal.time.start, 'minutes').format('HH:mm')}
                           -
                         {moment.utc().startOf('day').add(deal.time.end, 'minutes').format('HH:mm')}
                       </Typography>
                     );
                   })}

                 <DeleteButton variant="deal" id={deal._id} />
               </Typography>
               <br />
             </CardContent>
           </Card>
         </div>
       );
     });
   }

  public render() {
    return (
      <div>
        <Alert color="info">
          You are viewing the {this.state.barName} happy hours for drink {this.state.drink.name}
        </ Alert>
        <br />
        Original Price: ${this.state.drink.price}
        {this.renderDeals()}
    </div>
    );
  }
}
