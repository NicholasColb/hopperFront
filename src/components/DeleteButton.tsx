import * as React from 'react';
import Button from '@material-ui/core/Button';
import DeleteForeverIcon from '@material-ui/icons/DeleteForever';
import { apiManager } from '../modules/APIManager';

export interface Props {
  token: string;
  variant: 'deal' | 'drink' | 'bar';
  id: string;
}

export default class DeleteButton extends React.Component<Props> {
  constructor(props: Props) {
    super(props);

  }

  delete(e: React.MouseEvent<HTMLElement>) {
    if (this.props.variant === 'deal') {
      if (confirm('Delete Deal?')) {

        apiManager.deleteDeal(this.props.id)
          .then(res => {
            alert(res.text());
          });
      }
    } else if (this.props.variant === 'drink') {
        if (confirm('Delete drink and all of its happy hours?')) {
            apiManager.editProduct(this.props.id, {
              deleted: true,
            });
        }
    }
  }

  public render() {
    return (
      <Button onClick={e => this.delete(e)} variant="contained" color="default">
        <DeleteForeverIcon />
      </Button>
    );
  }
}
