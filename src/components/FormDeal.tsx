
import * as React from 'react';
import  { Alert, Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { Link } from 'react-router-dom';
import Chip from '@material-ui/core/Chip';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import MaterialUiInput from '@material-ui/core/Input';
import Checkbox from '@material-ui/core/Checkbox';
import ListItemText from '@material-ui/core/ListItemText';
import { apiManager } from '../modules/APIManager';

import DatePicker  from 'react-datepicker';
import TimePicker from 'rc-time-picker';

import { ExpandedProduct } from '../types/index';

import * as moment from 'moment';
import * as H from 'history';
import 'react-datepicker/dist/react-datepicker.css';
import 'rc-time-picker/assets/index.css';

export interface Props {
  location: H.Location;
  token: string;
}

export interface State {
  price: number;
  dates: Array<moment.Moment>;
  days: any; //tslint:disable-line
  dateFormat: number;
  startTime: number;
  endTime: number;
  drink?: ExpandedProduct;
}

const weekDays = [
  'Monday',
  'Tuesday',
  'Wednesday',
  'Thursday',
  'Friday',
  'Saturday',
  'Sunday',
];

export default class FormDeal extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = { price: 0, dates: [], days: [], dateFormat: 0, startTime: 0, endTime: 0};

  }

  componentWillMount() {
    this.setState({drink: this.props.location.state.drink});
  }

  handlePriceChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ price: Number(event.currentTarget.value) });
  }

  handleChipDelete = (data: React.FormEvent, date: moment.Moment) => {
    const chipData = [...this.state.dates];
    const chipToDelete = chipData.indexOf(date);
    chipData.splice(chipToDelete, 1);

    this.setState({dates: chipData});

  }

  handleRecurringDateChips = (event: React.ChangeEvent<HTMLSelectElement>) => {
    console.log(typeof ['']);  //tslint:disable-line
    this.setState({ days: event.target.value });
  }

  handleSpecificDateChange = (date: moment.Moment) => {

    const checkDoubleDateSelect = this.state.dates.find( (e: moment.Moment) => {
      return e.format('ll') === date.format('ll');
      } );
    if (date && !checkDoubleDateSelect) {

      this.setState({
          dates: this.state.dates.concat(date)
      });
    }
  }

  handleTimeStartChange = (time: moment.Moment) => {
      this.setState({
          startTime: time.hour() * 60 + time.minute()
      });

  }

  handleTimeEndChange = (time: moment.Moment) => {
      this.setState({
          endTime: time.hour() * 60 + time.minute()
      });
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  handleClick(): void {
    if (
      this.state.drink
      && this.state.price
      && this.state.days.length !== 0
      && this.state.endTime
      && this.state.startTime
    ) {
      this.postDeal();
    }
  }

  postDeal() {
    // const daysArr = this.state.days.map((x: string) => weekDays.indexOf(x).toString());
    /*let dateStart = moment().toISOString();
    let dateEnd = moment().add(14, 'days').toISOString();
    let timeStart = this.state.startTime.toISOString();
    let timeEnd = this.state.endTime.toISOString();
    */
    const params = {
        price: this.state.price,
        period: {
          start: moment().format('YYYY-MM-DD'),
          end: '2018-10-15',
        }, // dateStart,
          // dateEnd,
        time: {
          start: this.state.startTime,
          end: this.state.endTime
        }, // timeStart, // timeEnd,
        days:  this.state.days.map((x: string) => weekDays.indexOf(x).toString()),
        product: this.state.drink ? this.state.drink._id : '',
    };

    apiManager.postDeal(params)
      .then(res => {
        alert(res.text());
      });
  }

  renderSpecificDateChips = () => {

     return this.state.dates.map( (date: moment.Moment) => {

       return (

         <Chip
             key={this.state.dates.indexOf(date)}
             label={date.format('dddd, MMMM Do YYYY')}
             onDelete={e => this.handleChipDelete(e, date)}
             color="primary"
         />

       );
     });
   }

   renderDateSelection = (value: number) => {
     if (value) {
       return (
         <div>
           <DatePicker
             placeholderText="Click me"
             id="inputDateStart"
             autoComplete="off"
             onChange={this.handleSpecificDateChange}
           />

           <small id="PriceHelp" className="form-text text-muted">
              Select the dates of the happy hour
           </small>
           {this.renderSpecificDateChips()}
       </div>

       );
     } else {

       return (

           <Select
             multiple={true}
             value={this.state.days}
             onChange={this.handleRecurringDateChips}
             input={<MaterialUiInput id="select-multiple-checkbox" />}
             renderValue={( selected: Array<string> ) => (
               <div>
                 {selected.map( (day: string) => {
                   return (
                     <Chip key={day} label={day} />
                   );
                 })}
               </div>

             )}
           >
             {weekDays.map( (day: string) => {
               return (
                 <MenuItem
                   key={day}
                   value={day}
                 >
                   <Checkbox checked={this.state.days.indexOf(day) > -1} />
                   <ListItemText primary={day} />
                 </MenuItem>
               );
             })}
           </Select>

       );
     }
   }

   handleDateFormatChange = (event: React.FormEvent, value: number) => {
     this.setState({dateFormat: value});
   }

  public render() {
    return (
      <div>
        <Link to="/3">Back</Link>

      <Form onSubmit={this.handleSubmit}>

        <Alert color="info">
          You are adding a new happy hour for
          {this.state.drink ? ' ' + this.state.drink.name + ', $' + this.state.drink.price : null}
        </Alert>
        <FormGroup>

          <AppBar position="static" color="default" >
            <Tabs value={this.state.dateFormat} onChange={this.handleDateFormatChange}>
              <Tab label="Recurring Every Week" />
              <Tab label="Specific Days Only" />
            </Tabs>
          </AppBar>
          <br />

          {this.renderDateSelection(this.state.dateFormat)}

          <br />
          <br />

          <Label for="inputProductPrice"> Price: </Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">€</InputGroupAddon>
            <Input placeholder="Price" min="0" type="number" step="0.1" onChange={e => this.handlePriceChange(e)} />
          </InputGroup>
          <small id="PriceHelp" className="form-text text-muted">
            Enter the discounted happy hour price of the drink
          </small>
          <br />

          <Label for="inputTimeStart">The happy hour is from &nbsp; </Label>
          <TimePicker defaultValue={moment()} showSecond={false} onChange={this.handleTimeStartChange} />
          <Label for="inputTimeStart">&nbsp; to &nbsp; </Label>
          <TimePicker defaultValue={moment().add(2, 'hours')} showSecond={false} onChange={this.handleTimeEndChange}/>

        </FormGroup>

        <Button type="submit" className="btn btn-primary" onClick={() => this.handleClick()}>Submit</Button>

      </Form>
    </div>
    );
  }
}
