import * as React from 'react';
import Map from '../containers/Map';
import AllBarInfo from '../containers/AllBarInfo';
import AddressInput from '../containers/AddressInput';
import { addressBarStyle } from './MainMapPageStyles';

export interface Props {

}

export default class MainMapPage extends React.Component<Props> { 
  constructor(props: Props) {
    super(props);
  }

  render() {
   return (
    <div>
      <div style={addressBarStyle}>
        <AddressInput />
      </div>
      <div>
        <Map />
        <AllBarInfo />
      </div>
    </div>
   );
  }
}
