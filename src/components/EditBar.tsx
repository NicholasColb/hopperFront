import * as React from 'react';
import { Bar, Coords } from '../types/index';
import { apiManager } from '../modules/APIManager';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import { Suggest } from 'react-geosuggest';
import Geosuggest from 'react-geosuggest';
import * as H from 'history';

export interface Props {
  token: string;
  location: H.Location;
}

export interface State {
  bar: Bar;
  newName: string;
  newDescription: string;
  newLocation: Coords;
  newImage: string;
}

export default class EditBar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

  }

  componentWillMount() {
    var bar = this.props.location.state.bar;
    this.setState({
      bar: bar,
      newName: bar.name,
      newDescription: bar.description,
      newLocation: {
        lat: bar.location.latitude,
        lng: bar.location.longitude
      }
    });

  }

  handleNameChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ newName: event.currentTarget.value });
  }

  handleDescriptionChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ newDescription: event.currentTarget.value });
  }

  handleImageChange(event: React.FormEvent<HTMLInputElement>): void {
    var comp = this;
    var reader = new FileReader();
    reader.addEventListener('load', function () {
        comp.setState({ newImage: reader.result });
    });
    reader.readAsDataURL(event.currentTarget.files![0]);
  }

  handleLocationChange(suggestion: Suggest): void {
    if (suggestion) {
      this.setState({newLocation: {lat: Number(suggestion.location.lat), lng: Number(suggestion.location.lng)} });
    }
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  handleClick(): void {
    if (this.state.newName && this.state.newDescription && this.state.newLocation) {
      this.editBar();
    }
  }

  editBar() {
    const params = {
      name: this.state.newName,
      description: this.state.newDescription,
      location: this.state.newLocation
    };
    
    return apiManager.editBar(this.state.bar._id, params)
      .then(res => {
        alert(res.text());
      });
  }

  public render() {
    return (
      <div>

        <Form onSubmit={this.handleSubmit}>

          <FormGroup>
            <Label for="inputProductName"> Name: </Label>
            <Input
              type="text"
              placeholder={this.state.bar.name}
              className="form-control"
              id="inputProductName"
              onChange={e => this.handleNameChange(e)}
            />
            <small id="nameHelp" className="form-text text-muted">
              Change your bar's name
            </small>
          </FormGroup>

          <FormGroup>
            <Label for="inputBarName"> Description: </Label>
            <Input
              type="textarea"
              placeholder={this.state.bar.description}
              className="form-control"
              id="inputBarDescription"
              onChange={e => this.handleDescriptionChange(e)}
            />
            <small id="DescriptionHelp" className="form-text text-muted">
              Change your bar's description
            </small>
          </FormGroup>

          <FormGroup>
              <Label for="inputFile">Image: </Label>
              <Input
                type="file"
                name="file"
                id="inputFile"
                accept=".jpg, .png, .jpeg"
                onChange={e => this.handleImageChange(e)}
              />
              <img id="img-upload" src={this.state.newImage} />
          </FormGroup>
          <FormGroup>
            <Label for="inputLocation">Location: </Label>
            <Geosuggest id="inputLocation" onSuggestSelect={(s) => this.handleLocationChange(s)} />
          </FormGroup>

          <Button type="submit" className="btn btn-primary" onClick={() => this.handleClick()}>Save Changes</Button>

        </Form>

      </div>
    );
  }

}
