export const barsPageContainer: React.CSSProperties = {
  position: 'absolute',
  bottom: 0,
  width: '100vw',
  height: '95vh',
  backgroundImage: 'linear-gradient(to bottom right, #5281c4 -20%, #0c1d35 75%)',

};
