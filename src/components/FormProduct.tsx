
import * as React from 'react';
import  { Alert, Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import { Bar } from '../types/index';
import * as H from 'history';
import { apiManager } from '../modules/APIManager';

export interface Props {
  location: H.Location;
  token: string;
}

export interface State {
  name: string;
  price: number;
  bar: Bar;
}

export default class FormProduct extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

  }

  componentWillMount() {
    this.setState({bar: this.props.location.state.bar});
  }

  handleNameChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ name: event.currentTarget.value });
  }

  handlePriceChange(event: React.FormEvent<HTMLInputElement>): void {
     const newPrice = event.currentTarget.value;
     if (Number(newPrice) > 0) {
       this.setState({ price: Number(newPrice) });
     }
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  handleClick(): void {
    if (this.state.name && this.state.price && this.props.token) {
      this.postProduct();
    }
  }

  postProduct() {
    const params = {
      name: this.state.name,
      price: this.state.price,
    };

    apiManager.postProduct(this.state.bar._id, params)
      .then(res => {
        alert(res.text());
      });
  }

  public render() {
    return (
      <div>
        <Alert color="info">
          You are adding a new product to {this.state.bar.name}
        </ Alert>

      <Form onSubmit={this.handleSubmit}>

        <FormGroup>
          <Label for="inputProductName"> Name: </Label>
          <Input
            placeholder="Eg. Mohito"
            type="text"
            className="form-control"
            id="inputProductName"
            onChange={e => this.handleNameChange(e)}
            onKeyPress={ event => {               // disable submitting the form on 'enter' keypress
                if (event.key === 'Enter') {
                  event.preventDefault();
                }
              }
            }
          />
          <small id="nameHelp" className="form-text text-muted">Enter the drink's name </small>
        </FormGroup>

        <FormGroup>
          <Label for="inputProductPrice"> Price: </Label>
          <InputGroup>
            <InputGroupAddon addonType="prepend">€</InputGroupAddon>
            <Input
              placeholder="Eg. 5.00"
              type="number"
              min="0.01"
              step="0.01"
              onChange={e => this.handlePriceChange(e)}
              onKeyPress={ event => {               // disable submitting the form on 'enter' keypress
                  if (event.key === 'Enter') {
                    event.preventDefault();
                  }
                }
              }
            />
          </InputGroup>
          <small id="PriceHelp" className="form-text text-muted">
            Enter the original, undiscounted price of the drink
          </small>
        </FormGroup>

        <Button type="submit" className="btn btn-primary" onClick={() => this.handleClick()}>Submit</Button>

      </Form>

    </div>
    );
  }
}
