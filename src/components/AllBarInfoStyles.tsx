
const barInfoPopUp: React.CSSProperties = {
  position: 'absolute',
  bottom: 0,
  padding: 20,
  backgroundImage: 'linear-gradient(to bottom right, #5281c4 -20%, #0c1d35 75%)',
  opacity: 0.1,
  width: '100%',
  maxWidth: '100%',
  height: '95vh',
  transition: 'opacity 0.5s, z-index 0.5s',
  zIndex: -1,

};

const barInfoPopUpVisible: React.CSSProperties = {
  ...barInfoPopUp,
  opacity: 0.9,
  zIndex: 3,
};

export {barInfoPopUp, barInfoPopUpVisible};
