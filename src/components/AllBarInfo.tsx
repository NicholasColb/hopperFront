import * as React from 'react';
import { barInfoPopUp, barInfoPopUpVisible } from './AllBarInfoStyles';
import { Product } from '../types/index';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';

export interface Props {
  boxVisibility: boolean;
  text: string;
  products: Array<Product>;
  closeInfoBox: () => {type: string};
}

export default class AllBarInfo extends React.Component<Props> {  
  constructor(props: Props) {
    super(props);
  }

  _handleClick  = () => {
    this.props.closeInfoBox();
  }

  _renderProducts = () => {
     const data = this.props.products;
     if (data.length) {
       return data.map( (product: Product) => {
         return (
           <Grid item={true} key={data.indexOf(product)}>
             <Card>
               <CardContent>
                  <Typography variant="headline" >
                    {product.name}
                  </Typography>
                  <Typography color="textSecondary">
                    ${product.price}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
       );
     });

   } else {
      return(
        <Grid item={true}>
          <Paper>
            <Typography>
              No deals at this time!
            </Typography>
          </Paper>
        </Grid>
      );
    }
   }

  render() {
   const style = this.props.boxVisibility ? barInfoPopUpVisible : barInfoPopUp;
   return (
     <div style={style}>

          <Typography
            variant="display1"
            component="h5"
            style={{ color : 'white', paddingTop : '40px', paddingBottom : '10px', textAlign : 'center', }}
          >
            {this.props.text}
          </Typography>
          <Grid container={true} direction="column" spacing={24} justify="flex-start" alignItems="center">
            {this._renderProducts()}
          </Grid>
      <img
        src="/back.svg"
        style={{filter: 'invert(100%)', position: 'absolute', top: 0, left: 7, padding: '10px', width : '50px' }}
        onClick={this._handleClick}
      />
    </div>
   );
  }
}
