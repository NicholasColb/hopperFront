import * as React from 'react';
import {
  greatPlaceStyle,
  greatPlaceStyleHover,
  infoBox,
  infoBoxHover,
  infoBoxAddOn,
  infoBoxAddOnHover
} from './hoverstyles';

interface Props {
  $hover: boolean;
  text: string;
}

export default class MyGreatPlaceWithHover extends React.Component<any,any> {  // tslint:disable-line

  constructor(props: Props) {
    super(props);
  }

  render() {
    const style: React.CSSProperties = this.props.$hover ? greatPlaceStyleHover : greatPlaceStyle;
    const style2: React.CSSProperties = this.props.$hover ? infoBoxHover : infoBox;
    const style3: React.CSSProperties = this.props.$hover ? infoBoxAddOnHover : infoBoxAddOn;
    return (
      <div className="hint" style={style} >
         <div>{this.props.text}</div>
         <div style={style2} className="hint__content">
         Сlick me
         </div>
         <div style={style3} />
      </div>
    );
  }
}
