import * as React from 'react';
import Geosuggest from 'react-geosuggest';
import { Suggest } from 'react-geosuggest';
import { Coords } from '../types/index';

export interface Props {
  goToAddress?: (location: Coords) => void;
}

export interface State {

}

export default class AddressInput extends React.Component<Props, State> {  
  constructor(props: Props) {
    super(props);
  }

  handleLocationChange(suggestion: Suggest): void {
    if (suggestion && this.props.goToAddress) {
      this.props.goToAddress({lat: Number(suggestion.location.lat), lng: Number(suggestion.location.lng)} as Coords);
    }
  }

  render() {
    return(
      <div>
        <Geosuggest placeholder="Search places..."onSuggestSelect={(s) => this.handleLocationChange(s)}/>
      </div>
    );
  }
}
