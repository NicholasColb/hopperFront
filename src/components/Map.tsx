import * as React from 'react';
import GoogleMap from 'google-map-react';
import MapChild from '../containers/MapChild';
import { ChangeEventValue, Coords, Bar } from '../types/index';

export interface Props {
  loading1: boolean;
  error1: Error;
  center1: Coords;
  zoom1: number;
  data1: Array<Bar>;
  onMapEvent: (center: Coords, zoom: number) => {type: string, center: Coords, zoom: number};
  onDataCall: (c: Coords) => Promise<any>; //tslint:disable-line
}

export default class Map extends React.Component<Props> {
  constructor(props: Props) {
    super(props);
  }

  _onBoundsChange = ({center, zoom, bounds, marginBounds, size}: ChangeEventValue) => {
     this.props.onMapEvent(center, zoom);
     if (zoom > 12) {
       this.props.onDataCall(center);
     }
   }

  renderBars = (data: Array<Bar>) => {
     return data.map( (bar: Bar) => {
       return (

         <MapChild
           key={bar._id}
           lat={bar.location.latitude}
           lng={bar.location.longitude}
           text={bar.name}
           price={bar.products[0] ? '$' + bar.products[0].price : '$25'}
           products={bar.products}
           $hover={false}
         />

       );
     });
   }
public render() {
  if (this.props.error1) {
     return <div>Error! {this.props.error1.message}</div>;
   }

  return (
     <div style={{ bottom: 0 }}>

       <div style={{ width: '100vw', height: '95vh', position: 'absolute', bottom: 0}} >
         <GoogleMap
           bootstrapURLKeys={{ key: 'AIzaSyDJExL-z42xnUk0P6326zEdRuIE1aEAqlk', v: '3.31' }}
           center={this.props.center1}
           zoom={this.props.zoom1}
           onChange={this._onBoundsChange}
           hoverDistance={40}
         >

           {this.renderBars(this.props.data1)}

         </GoogleMap>
       </div>
   </div>
   );

}

}
