import * as React from 'react';
import { Route, Redirect } from 'react-router-dom';

const PrivateRouteBase = ({component, isAuthenticated, path}: any) => {    //tslint:disable-line
    const routeComponent = (props: any) => (          //tslint:disable-line
        isAuthenticated
            ? React.createElement(component, props)
            : <Redirect to={{pathname: '/signin'}}/>
    );
    return <Route path={path} render={routeComponent}/>;
};

export interface Props {
  isAuthed?: boolean;
  component: any; //tslint:disable-line
  path: string;
}

class PrivateRoute extends React.Component<any, any> { //tslint:disable-line
  constructor(props: Props) {
    super(props);
  }

  render() {
    return (
      <PrivateRouteBase
        component={this.props.component}
        path={this.props.path}
        isAuthenticated={this.props.isAuthed}
      />
    );
  }
}

export default PrivateRoute;
