
const K_SIZE = 40;

const greatPlaceStyle: React.CSSProperties = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: K_SIZE + 10,
  height: K_SIZE,
  left: -K_SIZE / 2,
  top: -K_SIZE / 2,

  border: '5px solid cyan',
  borderRadius: K_SIZE / 4,
  backgroundColor: 'white',
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  // fontWeight: 'bold',
  padding: 4,
  cursor: 'pointer',
  transition: '0.5s'
};

const greatPlaceStyleHover: React.CSSProperties = {
  ...greatPlaceStyle,
  border: '5px solid #3f51b5',
  color: '#f44336',
  zIndex: 1
};

const infoBox: React.CSSProperties = {
  position: 'relative',
  top: -60,
  width: 80,
  backgroundColor: 'cyan',
  borderRadius: '5px',
  textAlign: 'center',
  fontSize: 16,
  padding: 4,
  opacity: 0,
  transition: 'opacity 0.5s'

};

const infoBoxHover: React.CSSProperties = {
  ...infoBox,
  opacity: 1
};

const infoBoxAddOn: React.CSSProperties = {
    width: 0,
    height: 0,
    borderLeft: '5px solid transparent',
    borderRight: '5px solid transparent',
    borderTop: '10px solid cyan',
    position: 'relative',
    top: -63,
    opacity: 0,
    transition: 'opacity 0.5s'
};

const infoBoxAddOnHover: React.CSSProperties = {
  ...infoBoxAddOn,
  opacity: 1
};

export {greatPlaceStyle, greatPlaceStyleHover, infoBox, infoBoxHover, infoBoxAddOn, infoBoxAddOnHover, K_SIZE};
