import * as React from 'react';
import { GoogleLogin } from 'react-google-login';
import { GoogleLogout } from './SignOut';
import { UserData } from '../types/index';
import { Redirect } from 'react-router-dom';

interface Props  {
  userData: string;
  isAuthenticated: boolean;
  onSignIn: ( userData: UserData ) => void;
  onSignOut: () => void;
}

export default class SignIn extends React.Component<any,any> {  //tslint:disable-line
  constructor(props: Props) {
    super(props);
    this.state = ({redirect: false});
  }

  componentWillReceiveProps(nextProps: Props) {
    const newValue = nextProps.userData;
    if (newValue !== this.props.calculatedValue && newValue !== '') {
      this.setState({redirect: true});
    }
  }

  _onSignIn = (response: any) => { //tslint:disable-line

    const userData = {token: response.tokenId, name: response.profileObj.name} as UserData;

    this.props.onSignIn(userData);
  }

  _onSignOut = (response: any) => { //tslint:disable-line
    this.props.onSignOut();
  }

  onNotSignIn(response: any) { //tslint:disable-line
    console.log(response); //tslint:disable-line
  }
  onSignOut() {
    console.log('logged out'); //tslint:disable-line
  }

  renderButton(signedIn: boolean) {
    if (signedIn) {
      return (

        <GoogleLogout
          buttonText="logout"
          onLogoutSuccess={this._onSignOut}
        />

      );
    } else {
       return (

        <GoogleLogin

          style={
            {
              backgroundColor : '#4285F4',
              color : 'white',
              fontFamily : 'Roboto',
              paddingTop : '0.5em',
              paddingBottom : '0.5em',
              outline : 'none',
              width : '200px',
              boxShadow : 'none',
              border: '0',
            }
          }

          clientId="234697982810-2723k28c7lf16gbju8rm0hhduj9upg65.apps.googleusercontent.com"
          buttonText="Continue with Google"
          responseType="id_token"
          onSuccess={this._onSignIn}
          onFailure={this.onNotSignIn}
        />

      );
    }
  }

  render() {
    return (
      <div>
        {this.renderButton(this.props.isAuthenticated)}
       {this.state.redirect ? <Redirect to="/bars" /> : null}
      </div>

    );
  }
}
