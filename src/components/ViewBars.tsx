import * as React from 'react';
import { Alert } from 'reactstrap';
import { ExpandedBar } from '../types/index';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import LocalDrinkIcon from '@material-ui/icons/LocalBar';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';

import { barsPageContainer } from './ViewBarsStyles';
export interface Props {
  token: string;
  data: Array<ExpandedBar>;
  onDataCall: (t: string) => Promise<any>; //tslint:disable-line
}
export interface State {
}

export default class ViewBars extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  componentDidMount() {
   if (this.props.token) {
     this.props.onDataCall(this.props.token);

    } else {
      console.log('no idToken');  //tslint:disable-line
    }
  }

  renderBars(data: Array<ExpandedBar>) {
    return data.map((bar: ExpandedBar) => {
      return (
         <Grid item={true} key={data.indexOf(bar)}>
           <Card>
             <CardContent>
               <Typography variant="headline">
                {bar.name}
               </Typography>
               <Link
                  to={{
                    pathname: '/products',
                    state: { bar: bar , drinks: bar.products }
                  }}
               >
                 <Button  color="primary">
                  <LocalDrinkIcon />
                 </Button>
               </Link>
               &nbsp;
               &nbsp;
               <Link
                  to={{
                    pathname: '/editBar',
                    state: {bar: bar}
                  }}
               >
                 <Button  color="primary">
                  <EditIcon />
                 </Button>

               </Link>
              </CardContent>
            </Card>
        </Grid>

      );
    });
  }

  render() {
    return(
      <div style={barsPageContainer}>
        <Alert color="info">
          You are viewing your bars
        </Alert>
        <Grid
          container={true}
          direction="column"
          spacing={24}
          justify="flex-start"
          alignItems="center"
          style={{width: '100vw'}}
        >
          <Grid item={true}>
            <Link to="/newBar">
              <Button color="secondary" variant="contained">
                New Bar <AddIcon />
              </Button>
            </Link>
          </Grid>
          {this.renderBars(this.props.data)}
        </Grid>
      </div>
    );
  }
}
