/* global gapi */

import * as React from 'react';

interface Props {
  buttonText: string;
  onLogoutSuccess: () => void;
}

export class GoogleLogout extends React.Component<any,any> {  // tslint:disable-line
  constructor(props: Props) {
    super(props);
    this.state = {
      disabled: false
    };
    this.signOut = this.signOut.bind(this);
  }
  componentDidMount() {
      gapi.load('auth2', () => {
            gapi.auth2.init({client_id: '234697982810-2723k28c7lf16gbju8rm0hhduj9upg65.apps.googleusercontent.com'}).then(       // tslint:disable-line
              (res) => {
                this.setState({disabled: false});
              });

          }
      );
  }

  signOut() {
    const auth2 = gapi.auth2.getAuthInstance();
    if (auth2 != null) {
              auth2.signOut().then(
                  auth2.disconnect().then(this.props.onLogoutSuccess)
              );
    }
  }

  render() {

    return (
      <button
        onClick={this.signOut}
        style={
          {
            backgroundColor : '#ff5656',
            color : 'white',
            fontFamily : 'Roboto',
            paddingTop : '0.5em',
            paddingBottom : '0.5em',
            outline : 'none',
            width : '200px',
            boxShadow : 'none',
            border: '0',
          }
        }
      >
        Log out
      </button>

    );
  }
}
