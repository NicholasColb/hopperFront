
import * as React from 'react';
import { Alert } from 'reactstrap';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import EditIcon from '@material-ui/icons/Edit';
import DeleteButton from '../containers/DeleteButton';
import PlaylistAddIcon from '@material-ui/icons/PlaylistAdd';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';
import { ExpandedProduct } from '../types/index';
export interface Props {

}

export default class ViewProducts extends React.Component<any,any>{ // tslint:disable-line
  constructor(props: Props) {
    super(props);
    this.state = {drinks: []};

  }

  componentWillMount() {
    this.setState({bar: this.props.location.state.bar, drinks: this.props.location.state.drinks});
  }

  renderProducts = () => {
    const drinks = this.state.drinks;
    return drinks.map( (drink: ExpandedProduct) => {
       const style = drink.promotions.length ? { backgroundColor: '#5df46e' } : { backgroundColor: '#f25757'};
       return (
         <div key={drink._id} >
           <Card style={style}>
             <CardContent>
               <Typography variant="display2">
                   {drink.name}
                   &nbsp;
                   ${drink.price}
                   &nbsp;

                 <Link
                    to={{
                    pathname: '/editProduct',
                    state: {barName: this.state.bar.name, drink: drink }
                    }}
                 >
                  <Button variant="contained" color="secondary">
                   <EditIcon />
                  </Button>
                 </Link>

                 <Link
                    to={{
                    pathname: '/newDeal',
                    state: { drink: drink }
                    }}
                 >
                  <Button variant="contained" color="secondary">
                   <PlaylistAddIcon />
                  </Button>
                 </Link>

                 <DeleteButton variant="drink" id={drink._id} />
               </Typography>
               <br />
               <Typography variant="title">
                 {drink.promotions.length}&nbsp;happy hours set &nbsp;
                 {drink.promotions.length ?
                    <Link
                      to={{
                        pathname: '/deals',
                        state: {barName: this.state.bar.name, drink: drink}
                      }}
                    >View
                    </Link>
                   : null
                  }
               </Typography>
             </CardContent>
           </Card>

         </div>
       );
     });
   }

  public render() {
    return (
      <div>
        <Alert color="info">
          You are viewing your drinks at {this.state.bar.name}
        </Alert>

        <Link
           to={{
           pathname: '/newProduct',
           state: {bar: this.state.bar}
           }}
        >
        <Button variant="contained" color="primary">
          New Drink
          &nbsp;
          <AddIcon />
        </Button>
        </Link>

        {this.renderProducts()}
    </div>
    );
  }
}
