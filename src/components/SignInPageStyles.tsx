
export const loginPageContainer: React.CSSProperties = {
  position: 'absolute',
  bottom: 0,
  width: '100vw',
  height: '95vh',
  backgroundImage: 'linear-gradient(to bottom right, #5281c4 -20%, #0c1d35 75%)',

};

export const headlineStyle: React.CSSProperties = {
    color: 'white',
    textAlign: 'center',
    position: 'absolute',
    top: '10vh',
    marginLeft: 'auto',
    marginRight: 'auto',
    left: 0,
    right: 0,
};

export const cardStyle: React.CSSProperties = {
    width: '40vw',
    height: '50vh',
    position: 'absolute',
    top: '25vh',
    marginLeft: 'auto',
    marginRight: 'auto',
    left: 0,
    right: 0,
    paddingTop: '5vh',
    paddingBottom: '5vh',
    textAlign: 'center',
};

export const lockIconStyle: React.CSSProperties = {
    width: '10vw',
    maxWidth: '100px',
    height: 'auto',
    opacity: 0.6,
    position: 'absolute',
    bottom: 30,
    left: 0,
    right: 0,
    marginRight: 'auto',
    marginLeft: 'auto',

};

export const loginGuideStyle: React.CSSProperties = {
  position: 'absolute',
  left: 0,
  right: 0,
  bottom: 150,
  marginRight: 'auto',
  marginLeft: 'auto',

};
