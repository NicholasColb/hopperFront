
import * as React from 'react';
import  { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Geosuggest from 'react-geosuggest';
import { Suggest } from 'react-geosuggest';     // The type declaration of a Suggest Object
import { Link } from 'react-router-dom';
import { Coords } from '../types/index';
import { apiManager } from '../modules/APIManager';

export interface Props {
  token: string;
}

export interface State {
  name: string;
  description: string;
  image: string;
  location: Coords;
}

export default class FormBar extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {name: '', description: '', image: '', location: {lat: 0, lng: 0}};

  }

  handleNameChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ name: event.currentTarget.value });
  }

  handleDescriptionChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ description: event.currentTarget.value });
  }

  handleImageChange(event: React.FormEvent<HTMLInputElement>): void {
    var comp = this;
    var reader = new FileReader();
    reader.addEventListener('load', function () {
        comp.setState({ image: reader.result });
    });
    reader.readAsDataURL(event.currentTarget.files![0]);
  }

  handleLocationChange(suggestion: Suggest): void {
    if (suggestion) {
      this.setState({location: {lat: Number(suggestion.location.lat), lng: Number(suggestion.location.lng)} });
    }
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  handleClick(): void {
    if (this.state.name && this.state.description && this.state.location) {
      this.postBar();
    }
  }

  postBar() {
    const params = {
        name: this.state.name,
        description: this.state.description,
        location: this.state.location,
    };

    apiManager.postBar(params)
      .then(res => {
        console.log(res) //tslint:disable-line
      });
  }

  handleKeyPress = (event: React.FormEvent<HTMLInputElement>) => {
      alert('enter klik');
  }

  public render() {
    return (
      <div>
        <Link to="/1">View the map!</Link>
        <Link to="/3">View my bars!</Link>
      <Form onSubmit={this.handleSubmit}>
        <FormGroup>
          <Label for="inputBarName"> Name: </Label>
          <Input
            type="text"
            onKeyPress={ event => {               // disable submitting the form on 'enter' keypress
                if (event.key === 'Enter') {
                  event.preventDefault();
                }
              }
            }
            className="form-control"
            id="inputBarName"
            onChange={e => this.handleNameChange(e)}
          />
          <small id="nameHelp" className="form-text text-muted">Enter your bar's name </small>
        </FormGroup>
        <FormGroup>
          <Label for="inputBarName"> Description: </Label>
          <Input
            type="textarea"
            className="form-control"
            id="inputBarDescription"
            onChange={e => this.handleDescriptionChange(e)}
          />
          <small id="DescriptionHelp" className="form-text text-muted">Enter your bar's description </small>
        </FormGroup>
        <FormGroup>
            <Label for="inputFile">Image: </Label>
            <Input
              type="file"
              name="file"
              id="inputFile"
              accept=".jpg, .png, .jpeg"
              onChange={e => this.handleImageChange(e)}
            />
            <img id="img-upload" src={this.state.image} />
        </FormGroup>
        <FormGroup>
          <Label for="inputLocation">Location: </Label>
          <Geosuggest id="inputLocation" onSuggestSelect={(s) => this.handleLocationChange(s)} />
        </FormGroup>
        <Button type="submit" className="btn btn-primary" onClick={() => this.handleClick()}>Submit</Button>
      </Form>
      {this.state.location.lat}
      {this.state.location.lng}

    </div>
    );
  }
}
