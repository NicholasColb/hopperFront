import * as React from 'react';
import SignIn from '../containers/SignIn';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import LockIcon from '@material-ui/icons/Lock';
import {
  loginPageContainer,
  headlineStyle,
  cardStyle,
  lockIconStyle,
  loginGuideStyle,
} from './SignInPageStyles';

export interface Props {

}

export interface State {

}

export default class SignInPage extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
  }

  public render() {
    return (
      <div style={loginPageContainer}>
        <Typography style={headlineStyle} variant="display3">Hopper.</Typography>
        <Card style={cardStyle}>
          <CardContent>
            <SignIn />
            <LockIcon style={lockIconStyle} />
            <Typography style={loginGuideStyle}>You must be logged in to continue.</Typography>

          </CardContent>
        </Card>
      </div>
    );

  }

}
