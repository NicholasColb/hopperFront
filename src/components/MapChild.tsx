
import * as React from 'react';
import { Product } from '../types/index';
import {
  greatPlaceStyle,
  greatPlaceStyleHover,
  infoBox,
  infoBoxHover,
  infoBoxAddOn,
  infoBoxAddOnHover
} from './hoverstyles';

export interface Props {
  lat: number;
  lng: number;
  text: string;
  price: string | null;
  products: Array<Product>;
  $hover: boolean;
  showInfoBox: boolean;
  onInfoBoxCall?: (text: string, products: Array<Product>) => void;
}

class MapChild extends React.Component<any,any> {   //tslint:disable-line
  constructor(props: Props) {
    super(props);
    this.state = { showBox: false };
  }

  _handleClick = () => {
    this.props.onInfoBoxCall(this.props.text, this.props.products);
  }

  render() {
    const style = this.props.$hover ? greatPlaceStyleHover : greatPlaceStyle;
    const style2 = this.props.$hover ? infoBoxHover : infoBox;
    const style3 = this.props.$hover ? infoBoxAddOnHover : infoBoxAddOn;

    return (
    
      <div className="hint" style={style} onClick={this._handleClick}>
         <div>{this.props.price}</div>
         <div style={style2} className="hint__content">
         {this.props.text}
         </div>
         <div style={style3} />
      </div>

    );
  }
}

export default MapChild;
