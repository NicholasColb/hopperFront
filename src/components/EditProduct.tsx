import * as React from 'react';
import { Alert } from 'reactstrap';
import { ExpandedProduct } from '../types/index';
import { apiManager } from '../modules/APIManager';
import { Button, Form, FormGroup, Label, Input, InputGroup, InputGroupAddon } from 'reactstrap';
import * as H from 'history';
export interface Props {
  token: string;
  location: H.Location;
}

export interface State {
  drink: ExpandedProduct;
  barName: string;
  newName: string;
  newPrice: number;
}

export default class EditProduct extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);

  }

  componentWillMount() {
    const drink = this.props.location.state.drink;
    this.setState({
      barName: this.props.location.state.barName,
      drink: drink,
      newName: drink.name,
      newPrice: drink.price
    });
  }

  handleNameChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ newName: event.currentTarget.value });
  }

  handlePriceChange(event: React.FormEvent<HTMLInputElement>): void {
     this.setState({ newPrice: Number(event.currentTarget.value) });
  }

  handleSubmit(event: React.FormEvent<HTMLFormElement>): void {
    event.preventDefault();
  }

  handleClick(): void {
    if (this.state.newName && this.state.newPrice) {
      this.editProduct();
    }
  }

  editProduct() {
    const params = {
      name: this.state.newName,
      price: this.state.newPrice
    };
    
    return apiManager.editProduct(this.state.drink._id, params)
      .then(res => {
        alert(res.text());
      });
  }

  public render() {
    return (
      <div>
        <Alert color="info">
          You are editing ${this.state.drink.price} {this.state.drink.name} at {this.state.barName}
        </ Alert>
        <Form onSubmit={this.handleSubmit}>

          <FormGroup>
            <Label for="inputProductName"> Name: </Label>
            <Input
              type="text"
              placeholder={this.state.drink.name}
              className="form-control"
              id="inputProductName"
              onChange={e => this.handleNameChange(e)}
            />
            <small id="nameHelp" className="form-text text-muted">
              Change the drink's name
            </small>
          </FormGroup>

          <FormGroup>
            <Label for="inputProductPrice"> Price: </Label>
            <InputGroup>
              <InputGroupAddon addonType="prepend">€</InputGroupAddon>
              <Input
                placeholder={String(this.state.drink.price)}
                type="number"
                min="0"
                step="0.1"
                onChange={e => this.handlePriceChange(e)}
              />
            </InputGroup>
            <small id="PriceHelp" className="form-text text-muted">
              Change the original, undiscounted price of the drink
            </small>
          </FormGroup>

          <Button type="submit" className="btn btn-primary" onClick={() => this.handleClick()}>Save Changes</Button>

        </Form>

      </div>
    );
  }

}
