import { Coords, Bar, ExpandedBar, Period, Time } from '../types';

/* API subpaths */
enum APIPath {
  BARS = 'bars',
  MYBARS = 'myBars',
  EDITBAR = 'bar',
  NEWBAR = 'bar',
  EDITPRODUCT = 'product',
  NEWPRODUCT = 'product',
  NEWDEAL = 'deal',
  DELETEDEAL = 'deal'
}

export interface FetchError extends Error {
  res: Response;
}

interface BarEdit {
  name?: string;
  description?: string;
  location?: Coords;
  deleted?: boolean;
}
interface BarNew {
  name: string;
  description: string;
  location: Coords;
}

interface ProductEdit {
  name?: string;
  price?: number;
  deleted?: boolean;
}
interface ProductNew {
  name: string;
  price: number;
}

interface Deal {
  days: Array<string>;
  period: Period;
  price: number;
  time: Time;
  product: string;
}

export class APIManager {

  public get token() {
    return this._token;
  }
  
  public set token(token: string) {
    this._token = token;
  }

  private api: string;
  private _token: string;

  /* Rethrower for a catch block. Calls a function if a certain status
     code caused the error. */
  public static catchByStatus(code: number, action: (res: Response) => void) {
    return (e: FetchError) => {
      if (e.res && e.res.status === code) {
        action(e.res);
      }
      throw e; // Rethrow
    };
  }

  constructor(api: string = 'http://hophop.host/api') {

    if (!api) {
      throw new TypeError('API path cannot be falsey.');
    }

    if (api[api.length - 1] !== '/') {
      this.api = api + '/';
    } else  {
      this.api = api;
    }
  }

  public fetchBars(center?: Coords) {
    const url = this.url(APIPath.BARS);

    if (center) {
      url.searchParams.append('location.latitude', center.lat.toString() );
      url.searchParams.append('location.longitude', center.lng.toString() );
    }

    return this.fetch(url, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': this._token,
      }
    })
      .then(res => res.json())
      .then(res => res as Array<Bar>);
  }

  public fetchMyBars() {
    const url = this.url(APIPath.MYBARS);
    return this.fetch(url)
      .then(res => res.json())
      .then(res => res as Array<ExpandedBar>);
  }

  public postBar(payload: BarNew) {
    const url = this.url(APIPath.NEWBAR);
    const body = JSON.stringify({
      ...payload,
      location: {
        latitude: payload.location.lat,
        longitude: payload.location.lng,
      }
    });
    return this.fetch(url, {
      body,
    });
  }

  public postProduct(barId: string, payload: ProductNew) {
    const url = this.url(APIPath.NEWPRODUCT);
    const body = JSON.stringify({
      ...payload,
      bar: barId,
    });
    return this.fetch(url, {
      body,
    });
  }

  public postDeal(payload: Deal) {
    const url = this.url(APIPath.NEWDEAL);
    const body = JSON.stringify({
      ...payload,
    });
    return this.fetch(url, {
      body,
    });
  }

  public editBar(barId: string, payload: BarEdit) {
    const url = this.url(APIPath.EDITBAR, barId);

    const location = payload.location && {
      latitude: payload.location.lat,
      longitude: payload.location.lng,
    };

    const body = JSON.stringify({
      ...payload,
      location
    });
    
    return this.fetch(url, {
      method: 'PATCH',
      body,
    });
  }

  public editProduct(productId: string, payload: ProductEdit) {
    const url = this.url(APIPath.EDITPRODUCT, productId);

    const body = JSON.stringify(payload);
    
    return this.fetch(url, {
      method: 'PATCH',
      body,
    });
  }

  public deleteDeal(dealId: string) {
    const url = this.url(APIPath.DELETEDEAL, dealId);
    return this.fetch(url, {
      method: 'DELETE',
    });
  }

  /* Method to form the callable API url for a specific endpoint */
  private url(...path: string[]) {
    const url = new URL(`${this.api}${path.join('/')}`);
    console.log(url) // tslint:disable-line
    return url;
  }

  private fetch(url: URL, options: RequestInit = {}): Promise<Response> {

    const handleErrors = (res: Response) => {
      if (!res.ok) {
        const e: FetchError = {
          ...new Error(res.statusText),
          res,  
        };
        throw e;
      }
      return res;
    };

    return fetch(url.href, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': this._token,
      },
      ...options,
    })
      .then(handleErrors);
  }
}
export const apiManager = new APIManager();
