
export const SIGN_IN = 'SIGN_IN';
export type SIGN_IN = typeof SIGN_IN;

export const SIGN_OUT = 'SIGN_OUT';
export type SIGN_OUT = 'SIGN_OUT';

export const GOTO_ADDRESS = 'GOTO_ADDRESS';
export type GOTO_ADDRESS = typeof GOTO_ADDRESS;

export const SCROLL_MAP = 'SCROLL_MAP';
export type SCROLL_MAP = typeof SCROLL_MAP;

export const GET_INFO = 'GET_INFO';
export type GET_INFO = typeof GET_INFO;

export const CLOSE_INFO = 'CLOSE_INFO';
export type CLOSE_INFO = typeof CLOSE_INFO;

export const FETCH_DATA = 'FETCH_DATA';
export type FETCH_DATA = 'FETCH_DATA';

export const FETCH_PRODUCTS_BEGIN   = 'FETCH_PRODUCTS_BEGIN';
export type FETCH_PRODUCTS_BEGIN = typeof FETCH_PRODUCTS_BEGIN;

export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export type FETCH_PRODUCTS_SUCCESS = typeof FETCH_PRODUCTS_SUCCESS;

export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';
export type FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const FETCH_PRODUCTS = 'FETCH_PRODUCTS';
export type FETCH_PRODUCTS = 'FETCH_PRODUCTS';

export const FETCH_BARS_BEGIN   = 'FETCH_BARS_BEGIN';
export type FETCH_BARS_BEGIN = typeof FETCH_BARS_BEGIN;

export const FETCH_BARS_SUCCESS = 'FETCH_BARS_SUCCESS';
export type FETCH_BARS_SUCCESS = typeof FETCH_BARS_SUCCESS;

export const FETCH_BARS_FAILURE = 'FETCH_BARS_FAILURE';
export type FETCH_BARS_FAILURE = 'FETCH_BARS_FAILURE';

export const FETCH_BARS = 'FETCH_BARS';
export type FETCH_BARS = 'FETCH_BARS';
