import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import './index.css';
import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { mapReducer } from './reducers/index';
import { StoreState } from './types/index';
import MainMapPage from './components/MainMapPage';
import FormBar from './containers/FormBar';
import FormProduct from './containers/FormProduct';
import ViewBars from './containers/ViewBars';
import FormDeal from './containers/FormDeal';
import ViewProducts from './components/ViewProducts';
import ViewDeals from './components/ViewDeals';
import EditProduct from './containers/EditProduct';
import EditBar from './containers/EditBar';
import SignInPage from './components/SignInPage';
import PrivateRoute from './containers/PrivateRoute';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import { PersistGate } from 'redux-persist/integration/react';

const persistConfig = {
  key: 'root',
  storage,
  blacklist: ['wantsInfoBox', 'displayText', 'displayProducts']
};

const persistedReducer = persistReducer(persistConfig, mapReducer);

const store = createStore<StoreState>(
  persistedReducer,
  {
    isAuthenticated: true,
    userData: {token: '', name: ''},
    loading: false,
    error: null,
    center: {lat: 60.192059, lng: 24.945831},
    zoom: 10,
    publicData:
      [],
    privateData:
      [],
    wantsInfoBox: false,
    displayText: '',
    displayProducts: [],
  },
  applyMiddleware(thunk)
);

const persistor = persistStore(store);

ReactDOM.render(
  <div>
    <App />

    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Switch>
            <Route path="/signIn" component={SignInPage} />
            <Route path="/1" component={MainMapPage} />

            <PrivateRoute path="/newBar" component={FormBar} />
            <PrivateRoute path="/newDeal" component={FormDeal} />
            <PrivateRoute path="/bars" component={ViewBars} />
            <PrivateRoute path="/newProduct" component={FormProduct} />
            <PrivateRoute path="/products/" component={ViewProducts} />
            <PrivateRoute path="/editProduct" component={EditProduct}  />
            <PrivateRoute path="/editBar" component={EditBar}  />
            <PrivateRoute path="/deals" component={ViewDeals}  />

          </Switch>
        </Router>
      </PersistGate>
    </Provider>
  </div>
  ,
  document.getElementById('root') as HTMLElement
);
registerServiceWorker();
