import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Map from './containers/Map';

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Map />, div);
});
