// import { EnthusiasmAction } from '../actions';
// import { FetchData } from '../actions';
import { StoreState } from '../types/index';
import {
  SIGN_IN,
  SIGN_OUT,
  GOTO_ADDRESS,
  CLOSE_INFO,
  GET_INFO,
  SCROLL_MAP,
  FETCH_PRODUCTS_BEGIN,
  FETCH_PRODUCTS_SUCCESS,
  FETCH_PRODUCTS_FAILURE,
  FETCH_BARS_BEGIN,
  FETCH_BARS_SUCCESS,
  FETCH_BARS_FAILURE
} from '../constants/index';
import {
  AddressChangeAction,
  FetchAction,
  FetchBarsAction,
  ScrollAction,
  InfoBoxAction,
  SignAction
} from '../actions';

export function mapReducer(state: StoreState, action: AddressChangeAction|FetchAction|FetchBarsAction|ScrollAction|InfoBoxAction|SignAction): StoreState {  //tslint:disable-line
  switch (action.type) {
    case SIGN_IN:
      return {
        ...state,
        userData: action.userData,
        isAuthenticated: true
      };
    case SIGN_OUT:

      return {
        ...state,
       userData: {token: '', name: ''},
       isAuthenticated: false
      };
    case GOTO_ADDRESS:
      return {
        ...state,
        center: action.location,
        zoom: 15
      };

    case CLOSE_INFO:
      return {
        ...state,
        wantsInfoBox: false,
        displayText: '',
        displayProducts: []
      };

    case GET_INFO:
      return {
        ...state,
        wantsInfoBox: true,
        displayText: action.text,
        displayProducts: action.products
      };

    case SCROLL_MAP:
      const newCenter = action.center;
      const newZoom = action.zoom;

      return {
        ...state,
        center: newCenter,
        zoom: newZoom
      };

    case FETCH_PRODUCTS_BEGIN:
      // Mark the state as "loading" so we can show a spinner or something
      // Also, reset any errors. We're starting fresh.
      return {
        ...state,
        loading: true,
        error: null
      };

    case FETCH_PRODUCTS_SUCCESS:
      // All done: set loading "false".
      // Also, replace the items with the ones from the server
      return {
        ...state,
        loading: false,
        publicData: action.payload.products
      };

    case FETCH_PRODUCTS_FAILURE:
      // The request failed, but it did stop, so set loading to "false".
      // Save the error, and we can display it somewhere
      // Since it failed, we don't have items to display anymore, so set it empty.
      // This is up to you and your app though: maybe you want to keep the items
      // around! Do whatever seems right.
      return {
        ...state,
        loading: false,
        error: action.payload.error,
        publicData: []
      };
      case FETCH_BARS_BEGIN:
        // Mark the state as "loading" so we can show a spinner or something
        // Also, reset any errors. We're starting fresh.
        return {
          ...state,
          loading: true,
          error: null
        };

      case FETCH_BARS_SUCCESS:
        // All done: set loading "false".
        // Also, replace the items with the ones from the server
        return {
          ...state,
          loading: false,
          privateData: action.payload.bars
        };

      case FETCH_BARS_FAILURE:
        // The request failed, but it did stop, so set loading to "false".
        // Save the error, and we can display it somewhere
        // Since it failed, we don't have items to display anymore, so set it empty.
        return {
          ...state,
          loading: false,
          error: action.payload.error,
          privateData: []
        };

    default:
      // ALWAYS have a default case in a reducer
      return state;
  }
}
