import ViewBars from '../components/ViewBars';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect, Dispatch } from 'react-redux';

export function mapStateToProps({userData, privateData}: StoreState) {
  return {
    token: userData.token,
    data: privateData
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.FetchBarsAction>) {
  return {
    onDataCall: (t: string ) => dispatch(actions.fetchBars(t)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ViewBars);
