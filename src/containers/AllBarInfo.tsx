import AllBarInfo from '../components/AllBarInfo';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect, Dispatch } from 'react-redux';

export function mapStateToProps({wantsInfoBox, displayText, displayProducts}: StoreState) {
  return {
    boxVisibility: wantsInfoBox,
    text: displayText,
    products: displayProducts
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.InfoBoxAction>) {
  return {
    closeInfoBox: () => dispatch(actions.closeInfoBox())
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(AllBarInfo);
