import Map from '../components/Map';
import * as actions from '../actions/';
import { StoreState } from '../types/index';
import { connect, Dispatch } from 'react-redux';
import { Coords } from '../types/index';

export function mapStateToProps({ loading, error, center, zoom, publicData }: StoreState) {
  return {
    loading1: loading,
    error1: error,
    center1: center,
    zoom1: zoom,
    data1: publicData,
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.FetchAction | actions.ScrollAction>) {
  return {
    onDataCall: (c: Coords ) => dispatch(actions.fetchProducts(c)),
    onMapEvent: (center: Coords, zoom: number) => dispatch(actions.scrollMap(center, zoom)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);
