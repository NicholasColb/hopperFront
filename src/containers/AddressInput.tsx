import AddressInput from '../components/AddressInput';
import * as actions from '../actions/';
import { connect, Dispatch } from 'react-redux';
import { Coords } from '../types/index';
import { Props } from '../components/AddressInput';

export function mapDispatchToProps(dispatch: Dispatch<actions.AddressChangeAction>) {
  return {
    goToAddress: (location: Coords) => dispatch(actions.goToAddress(location)),
  };
}

export default connect<Props>(null, mapDispatchToProps)(AddressInput);
