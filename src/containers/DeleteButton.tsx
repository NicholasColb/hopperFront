import DeleteButton from '../components/DeleteButton';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';

export function mapStateToProps({userData}: StoreState) {
  return {
    token: userData.token
  };
}

export default connect(mapStateToProps)(DeleteButton);
