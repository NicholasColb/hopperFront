import PrivateRoute from '../components/PrivateRoute';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';

export interface Props {
  path: string;
  component: any; //tslint:disable-line
}

export function mapStateToProps({ loading, isAuthenticated }: StoreState, props: Props) {
  return {
    isAuthed: isAuthenticated,
    component: props.component,
    path: props.path,
  };
}

export default connect(mapStateToProps)(PrivateRoute);
