import SignIn from '../components/SignIn';
import * as actions from '../actions/';
import { UserData, StoreState } from '../types/index';
import { connect, Dispatch } from 'react-redux';

export function mapStateToProps({userData, isAuthenticated}: StoreState ) {
  return {
    userData: userData.token,
    isAuthenticated: isAuthenticated,
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.SignAction>) {
  return {
    onSignIn: (userData: UserData) => dispatch(actions.signIn(userData)),
    onSignOut: () => dispatch(actions.signOut()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
