import MapChild from '../components/MapChild';
import * as actions from '../actions/';
import { StoreState, Product } from '../types/index';
import { connect, Dispatch } from 'react-redux';

export interface Props {
  lat: number;
  lng: number;
  text: string;
  price: string | null;
  products: Array<Product>;
  $hover: boolean;
}

export function mapStateToProps({ wantsInfoBox }: StoreState, props: Props) {
  return {
    showInfoBox: wantsInfoBox,
    lat: props.lat,
    lng: props.lng,
    text: props.text,
    price: props.price,
    products: props.products,
    $hover: props.$hover,
  };
}

export function mapDispatchToProps(dispatch: Dispatch<actions.InfoBoxOpenAction>) {
  return {
    onInfoBoxCall: (text: string, products: Array<Product>) => dispatch(actions.showInfoBox(text, products)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MapChild);
