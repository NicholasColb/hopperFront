import EditBar from '../components/EditBar';
import { StoreState } from '../types/index';
import { connect } from 'react-redux';
import * as H from 'history';

export interface Props {
    location: H.Location;
}

export function mapStateToProps({userData}: StoreState, props: Props) {
  return {
    token: userData.token,
    location: props.location,
  };
}

export default connect(mapStateToProps)(EditBar);
