import * as constants from '../constants';
// import {StoreState} from '../types/index';
 // import {Action, ActionCreator} from 'redux';
import { Dispatch } from 'react-redux';
import { apiManager } from '../modules/APIManager';
import { Coords, Product, UserData } from '../types';

export * from './FetchBarsAction';
import { FetchBarsAction, fetchBarsBegin, fetchBarsSuccess } from './FetchBarsAction';

export * from './FetchAction';
import { FetchAction, fetchProductsBegin, fetchProductsSuccess } from './FetchAction';

export interface SignInAction {
  type: constants.SIGN_IN;
  userData: UserData;
}

export interface SignOutAction {
  type: constants.SIGN_OUT;
}

export type SignAction = SignInAction | SignOutAction;

export interface ScrollAction {
  type: constants.SCROLL_MAP;
  center: Coords;
  zoom: number;
}

export interface InfoBoxOpenAction {
  type: constants.GET_INFO;
  text: string;
  products: Array<Product>;
}

export interface InfoBoxCloseAction {
  type: constants.CLOSE_INFO;
}

export type InfoBoxAction = InfoBoxOpenAction | InfoBoxCloseAction;

export interface AddressChangeAction {
  type: constants.GOTO_ADDRESS;
  location: Coords;
}

export const signIn = (userData: UserData) => ({
  type: constants.SIGN_IN,
  userData
});

export const signOut = () => ({
  type: constants.SIGN_OUT,
});

export const goToAddress = (location: Coords) => ({
  type: constants.GOTO_ADDRESS,
  location
});

export const scrollMap = (center: Coords, zoom: number) => ({
    type: constants.SCROLL_MAP,
    center,
    zoom
});

export const showInfoBox = (text: string, products: Array<Product>) => ({
    type: constants.GET_INFO,
    text,
    products
});

export const closeInfoBox = () => ({
    type: constants.CLOSE_INFO,
});

/***************************************
  *******************
  *********
  ***
  * The async http requests of the application
  ***
  *********
  *******************
  **************************************
*/

export function fetchProducts(center: Coords) {
    return (dispatch: Dispatch<FetchAction>) => {
      dispatch(fetchProductsBegin());
      return apiManager.fetchBars(center)
        .then(json => {
          dispatch(fetchProductsSuccess(json));
          return json;
        });
    };
}

export function fetchBars(idtoken: string) {
    return (dispatch: Dispatch<FetchBarsAction>) => {
      dispatch(fetchBarsBegin());
      apiManager.token = idtoken;
      return apiManager.fetchMyBars()
        .then(json => {
          dispatch(fetchBarsSuccess(json));
          return json;
        });
    };
}
