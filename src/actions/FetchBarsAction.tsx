import * as constants from '../constants';
import { ExpandedBar } from '../types';

// Types and actions for fetching the bar owner's bars and updating the global redux store

export type  FetchBarsAction = FetchBarsBegin | FetchBarsSuccess | FetchBarsFailure;

export interface FetchBarsBegin {
    type: constants.FETCH_BARS_BEGIN;
}

export interface FetchBarsSuccess {
    type: constants.FETCH_BARS_SUCCESS;
    payload: {
      bars: Array<ExpandedBar>;
    };
}

export interface FetchBarsFailure {
    type: constants.FETCH_BARS_FAILURE;
    payload: {
      error: Error;
    };
}

export function fetchBarsBegin(): FetchBarsBegin {
  return {
    type: constants.FETCH_BARS_BEGIN
  };
}

export function fetchBarsSuccess(bars: Array<ExpandedBar>): FetchBarsSuccess {
  return {
    type: constants.FETCH_BARS_SUCCESS,
    payload: { bars }
  };
}

export function fetchBarsError(error: Error): FetchBarsFailure {
  return {
    type: constants.FETCH_BARS_FAILURE,
    payload: { error }
  };
}
