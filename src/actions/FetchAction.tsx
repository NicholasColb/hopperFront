import * as constants from '../constants';
import { Bar } from '../types';

// Types and actions for fetching the public bars and updating the global redux store
export interface FetchProductsBegin {
    type: constants.FETCH_PRODUCTS_BEGIN;
}

export interface FetchProductsSuccess {
    type: constants.FETCH_PRODUCTS_SUCCESS;
    payload: {
      products: Array<Bar>;
    };
}

export interface FetchProductsFailure {
    type: constants.FETCH_PRODUCTS_FAILURE;
    payload: {
      error: Error;
    };
}

export type FetchAction = FetchProductsBegin | FetchProductsSuccess | FetchProductsFailure;

export function fetchProductsBegin(): FetchProductsBegin {
  return {
    type: constants.FETCH_PRODUCTS_BEGIN
  };
}

export function fetchProductsSuccess(products: Array<Bar>): FetchProductsSuccess {
  return {
    type: constants.FETCH_PRODUCTS_SUCCESS,
    payload: { products }
  };
}

export function fetchProductsError(error: Error): FetchProductsFailure {
  return {
    type: constants.FETCH_PRODUCTS_FAILURE,
    payload: { error }
  };
}
