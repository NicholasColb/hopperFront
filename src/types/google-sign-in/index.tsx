interface AuthResponse {
   access_token: string;
   id_token: string;
   login_hint: string;
   scope: string;
   expires_in: number;
   first_issued_at: number;
   expires_at: number;
 }

interface BasicProfile {
  getId(): string;
  getName(): string;
  getGivenName(): string;
  getFamilyName(): string;
  getImageUrl(): string;
  getEmail(): string;
}

interface SigninOptions {
    /**
     * The package name of the Android app to install over the air.
     * See Android app installs from your web site:
     * https://developers.google.com/identity/sign-in/web/android-app-installs
     */
    app_package_name?: string;
    /**
     * 	Fetch users' basic profile information when they sign in.
     * 	Adds 'profile', 'email' and 'openid' to the requested scopes.
     * 	True if unspecified.
     */
    fetch_basic_profile?: boolean;
    /**
     * Specifies whether to prompt the user for re-authentication.
     * See OpenID Connect Request Parameters:
     * https://openid.net/specs/openid-connect-basic-1_0.html#RequestParameters
     */
    prompt?: string;
    /**
     * The scopes to request, as a space-delimited string.
     * Optional if fetch_basic_profile is not set to false.
     */
    scope?: string;
  }

interface SigninOptionsBuilder {
    setAppPackageName(name: string): () => void;
    setFetchBasicProfile(fetch: boolean): () => void;
    setPrompt(prompt: string): () => void;
    setScope(scope: string): () => void;
  }

export interface GoogleUser {
   /**
    * Get the user's unique ID string.
    */
   getId(): string;

   /**
    * Returns true if the user is signed in.
    */
   isSignedIn(): boolean;

   /**
    * Get the user's Google Apps domain if the user signed in with a Google Apps account.
    */
   getHostedDomain(): string;

   /**
    * Get the scopes that the user granted as a space-delimited string.
    */
   getGrantedScopes(): string;

   /**
    * Get the user's basic profile information.
    */
   getBasicProfile(): BasicProfile;

   /**
    * Get the response object from the user's auth session.
    */
   getAuthResponse(includeAuthorizationData?: boolean): AuthResponse;

   /**
    * Forces a refresh of the access token, and then returns a Promise for the new AuthResponse.
    */
   reloadAuthResponse(): Promise<AuthResponse>;

   /**
    * Returns true if the user granted the specified scopes.
    */
   hasGrantedScopes(scopes: string): boolean;

   /**
    * Signs in the user. Use this method to request additional scopes for incremental
    * authorization or to sign in a user after the user has signed out.
    * When you use GoogleUser.signIn(), the sign-in flow skips the account chooser step.
    * See GoogleAuth.signIn().
    */
   signIn(options?: SigninOptions | SigninOptionsBuilder): any;     // tslint:disable-line

   /**
    * See GoogleUser.signIn()
    */
   grant(options?: SigninOptions | SigninOptionsBuilder): any;    // tslint:disable-line

   /**
    * Get permission from the user to access the specified scopes offline.
    * When you use GoogleUser.grantOfflineAccess(), the sign-in flow skips the account chooser step.
    * See GoogleUser.grantOfflineAccess().
    */
   grantOfflineAccess(scopes: string): void;

   /**
    * Revokes all of the scopes that the user granted.
    */
   disconnect(): void;
 }
