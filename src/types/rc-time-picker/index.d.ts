
declare module 'rc-time-picker' {  // tslint:disable-next-line:max-line-length

export interface Props {
    prefixCls?: string;
    clearText?: string;
    value?: object;
    defaultOpenValue?: object,
    inputReadOnly?: boolean;
    disabled?: boolean;
    allowEmpty?: boolean;
    defaultValue?: object;
    open?: boolean;
    defaultOpen?: boolean;
    align?: object;
    transitionName?: string;
    getPopupContainer?: () => void;
    placeholder?: string;
    format?: string;
    showHour?: boolean;
    showMinute?: boolean;
    showSecond?: boolean;
    style?: object;
    className?: string;
    popupClassName?: string;
    disabledHours?: () => void;
    disabledMinutes?: () => void;
    disabledSeconds?: () => void;
    hideDisabledOptions?: boolean;
    onChange?: (x:any) => void; // tslint:disable-line
    onOpen?: () => void;
    onClose?: () => void;
    onFocus?: () => void;
    onBlur?: () => void;
    addon?: () => void;
    name?: string;
    autoComplete?: string;
    use12Hours?: boolean;
    hourStep?: number;
    minuteStep?: number;
    secondStep?: number;
    focusOnOpen?: boolean;
    onKeyDown?: () => void;
    autoFocus?: boolean;
    id?: string;

}

export default class TimePicker extends React.Component<Props> {}

}
