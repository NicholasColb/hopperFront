export interface StoreState {
    userData: UserData;
    loading: boolean;
    error: any; // tslint:disable-line
    center: Coords;
    zoom: number;
    publicData: Array<Bar>;
    privateData: Array<ExpandedBar>;
    wantsInfoBox: boolean;
    displayText: string;
    displayProducts: Array<Product>;
    isAuthenticated: boolean;
}

export interface Bounds {
  nw: Coords;
  ne: Coords;
  sw: Coords;
  se: Coords;
}

export interface Point {
  x: number;
  y: number;
}

export interface Coords {
  lat: number;
  lng: number;
}

export interface Size {
  width: number;
  height: number;
}

export interface ChangeEventValue {
  center: Coords;
  zoom: number;
  bounds: Bounds;
  marginBounds: Bounds;
  size: Size;
}
export interface Bar {
    _id: string;
    name: string;
    description: string;
    location: Coordinates;
    products: Array<Product>;
    added: string;
    verified: boolean;
}

export interface ExpandedBar {
  _id: string;
  name: string;
  location: Coordinates;
  products: Array<ExpandedProduct>;
  added?: string;
  deleted?: boolean;
  verified: boolean;
}

export interface Product {
  name: string;
  oldPrice: number;
  price: number;
}

export interface ExpandedProduct {
  _id: string;
  name: string;
  price: number;
  promotions: Array<Promotion>;
  deleted?: boolean;
  added?: string;
  modified?: string;
}

export interface Promotion {
  days: Array<string>;
  period: Period;
  price: number;
  time: Time;
  _id: string;
}

export interface Period {
  start: string;
  end: string;
}

export interface Time {
  start: number;
  end: number;
}

export interface UserData {
  token: string;
  name: string;
}
