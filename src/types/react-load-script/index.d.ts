
declare module 'react-load-script' {  // tslint:disable-next-line:max-line-length

export interface Props {
  url: string;
}

export default class Script extends React.Component<Props> {}
}
