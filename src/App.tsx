import * as React from 'react';
import ResponsiveDrawer from './components/ResponsiveDrawer';
import './App.css';

class App extends React.Component<any,any> { //tslint:disable-line
  render() {
    return (

        <ResponsiveDrawer />
    
    );
  }
}

export default App;
